using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4_RPPOON
{
    class Program
    {	// 1, 2 i 3 ZAD
        static void Main(string[] args)
        {
			Dataset readFile = new Dataset("LV4.txt");
			Analyzer3rdParty analyzer3RdParty = new Analyzer3rdParty();
			Adapter adapter = new Adapter(analyzer3RdParty);
		

			double[] averagePerColumn = adapter.CalculateAveragePerColumn(readFile);
			double[] averagePerRow = adapter.CalculateAveragePerRow(readFile);

			for(int i = 0; i < averagePerColumn.Length; i++)
			{
				Console.WriteLine(averagePerColumn[i]);
			}
			for (int j = 0; j < averagePerRow.Length; j++)
			{
				Console.WriteLine(averagePerRow[j]);
			}
			
			
            //3 ZAD
            RentingConsolePrinter print = new RentingConsolePrinter();
			List<IRentable> prints = new List<IRentable>();
			prints.Add(new Book("Zločin i kazna"));
			prints.Add(new Video("MC Stojan-MOGU JA TO"));
			Console.WriteLine("Cijena:");
			print.PrintTotalPrice(prints);
			Console.WriteLine("Na listi su:");
			print.DisplayItems(prints);


		}
	}
}
