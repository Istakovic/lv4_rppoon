using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV4_RPPOON
{
    class Adapter:IAnalytics
    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }
        private double[][] ConvertData(Dataset dataset)
        {
            IList<List<double>> Source = dataset.GetData();
            int rowCount = Source.Count;
            int columnCount = Source[0].Count;
            double[][] data = new double[rowCount][];
            
                for(int i = 0; i < columnCount; i++)
            {
                data[i] = new double[columnCount];
            }
                for(int i = 0; i < rowCount; i++)
                     for (int j = 0; j < columnCount; j++)
                    data[i][j] = Source[i][j];
            
            return data;
            //moramo alocirati memoriju za polje te kopirati 2D listu u 2D polje
        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }
    }
}
